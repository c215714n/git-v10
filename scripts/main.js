/* Constantes y Variables */
    const d = document
    menuLst = d.querySelector('nav .menu')
    menuBtn = d.querySelector('nav .btn')
    mapFrm = d.querySelector('.map iframe')
    mapBtn = d.querySelector('.map .btn')

    menuBtn.onclick = () => menuLst.classList.toggle('active') ? menuBtn.innerHTML = '&times;' : menuBtn.innerHTML = '&equiv;'
    
    const fullSize = () => {
            mapFrm.classList.toggle('active') ?
                mapBtn.classList.replace('icon-max','icon-min') :
                mapBtn.classList.replace('icon-min', 'icon-max')
        }
    mapBtn.addEventListener('click', () => fullSize() )
    